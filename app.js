var express = require('express')
  , routes = require('./util/routes')
  , config = require('./config.json')
  , app = express()
  , oneDay = 86400000

require('./util/dust-helpers')(require('consolidate').dust)

app.use(express.compress())
app.use('/assets', express.static(process.cwd()+'/client/assets', { maxAge: oneDay*30 }))
app.use('/polyfills', express.static(process.cwd()+'/client/scripts/polyfills', { maxAge: oneDay*30 }))
app.use('/build', express.static(process.cwd()+'/build', { maxAge: oneDay*30 }))

app.use(express.bodyParser())
app.use(express.cookieParser())
app.use(express.session({ secret: 'HNE(:WF40w3fn9835u39uinDFg'}))

app.use(function(req, res, next) {
	res.renderStack = function(templates, data, fn) {
		data = data || {}
		r(null, '')
		
		function r(err, html) {
			if(err) return fn && fn(err)
			
			var template = templates.splice(0, 1)[0]
			data.content = html
			res.render(template, data, templates.length ? r : fn)
		}
	}

	req.routeMatch = function() {
		var start = new RegExp('^https?:\\/\\/'+req.host+'(:\\d+)?')
		return routes.matches(req.get('referrer').replace(start, ''), req.path)
	}
	next()
})

app.use(function(req, res, next) {
	res.locals.url = req.path
	res.locals.site_domain = req.protocol + '://' + req.host + ':3000'
	try {
		res.locals.address = req.session.address = req.cookies.address && JSON.parse(req.cookies.address)
	} catch(e) {
		res.cookie('address', '', { expires: new Date(1), path: '/' })
	}
	try {
		res.locals.order = req.session.order = (req.cookies.order && JSON.parse(req.cookies.order)) || { restaurants:[], subtotal:0, items:0 }
	} catch(e) {
		res.locals.order = req.session.order = { restaurants:[], subtotal:0, items:0 }
		res.cookie('order', JSON.stringify({ restaurants:[], subtotal:0, items:0 }), { expires: new Date(1), path: '/' })
	}
	res.locals.account = req.session.account
	res.locals.startup_time = Date.now()
	next()
})

app.use(require('./routers/main'))
app.use(require('./routers/order'))
app.use(require('./routers/location'))

app.listen(config.port)
console.log('site started on port' + config.port)