var Throttle = module.exports = function(delay) {
	this.executing = false
	this.timeout = 0
	this.delay = delay
}

Throttle.prototype.exec = function(fn) {
	var done = this.done.bind(this)
	  , throttle = this

	if(!this.executing && !this.timeout) {
		this.executing = true
		fn(done)
	} else {
		clearTimeout(this.timeout)
		this.timeout = setTimeout(function() {
			throttle.timeout = 0
			throttle.executing = true
			fn(done)
		}, this.delay)
	}
}

Throttle.prototype.done = function() {
	if(!this.timeout) this.executing = false
}