var currency = module.exports

currency.format = function(value, options) {
	if(!value) value = 0.00

	var formatted = value.toFixed(2).replace(/^(-?)/, '$1$')
	options = options || {}

	if(options.force_sign && value >= 0) formatted = '+' + formatted

	if(!Math.round(value*100)) formatted.replace('-', '')

	return formatted
}

currency.parse = function(value) {
	return parseFloat(value.replace('$', ''))
}