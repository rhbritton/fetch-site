module.exports = function getCurrentStatus(hours_text, constrain_text) {
	if(!hours_text) return {}
	
	var weekly_hours = getHours(hours_text)
	  , today = new Date()
	  , day = today.getDay()
	  , next_hours
	  , todays_hours
	  , text
	  , open = false
	  , until

	if(constrain_text) {
		weekly_hours = constrain(weekly_hours, getHours(constrain_text))
	}

	if(weekly_hours[day] && weekly_hours[day].length) {
		todays_hours = getOpenClose(weekly_hours[day])
		if(today > todays_hours.open && today < todays_hours.close) {
			open = true
			until = todays_hours.close
			text = 'Order until '+todays_hours.close_string
		} else if(today < todays_hours.open) {
			text = 'Unavailable until '+todays_hours.open_string
		} else {
			next_hours = getOpenClose(getNextOpenDay(weekly_hours, day))
			if(!next_hours) text = 'Unavailable'
			else text = 'Unavailable until '+next_hours.day+' at '+next_hours.open_string
		}
	} else {
		next_hours = getOpenClose(getNextOpenDay(weekly_hours, day))
		if(!next_hours) text = 'Unavailable'
		else text = 'Unavailable until '+next_hours.day+' at '+next_hours.open_string
	}

	return { open:open, text:text }
}

function getOpenCloseSlot(day) {
	var open = new Date()
	  , close = new Date()
	open.setHours(day.open.hours)
	open.setMinutes(day.open.minutes)
	open.setSeconds(0)
	close.setHours(day.close.hours)
	close.setMinutes(day.close.minutes)
	close.setSeconds(0)
	if(close < open) {
		close.setDate(close.getDate()+1)
	}
	return { day:day.day, open:open, close:close, open_string:day.open_string, close_string:day.close_string }
}

function getOpenClose(day) {
	var slot
	  , today = new Date()

	if(!day) return

	for(var i=0; i< day.length; i++) {
		slot = getOpenCloseSlot(day[i])
		if(today < slot.close) {
			return slot
		}
	}
	
	return slot
}

function getNextOpenDay(hours, i) {
	var day, original = i

	i = (i+1)%7

	while(i != original) {
		if(hours[i] && hours[i].length) 
			return hours[i]
		i = (i+1)%7
	}
}

function constrain(a, b) {
	var i, DAYS_IN_A_WEEK = 7, hours = []
	
	for(i = 0; i < DAYS_IN_A_WEEK; i++) {
		hours[i] = constrainDay(a[i], b[i])
	}

	return hours
}

function constrainDay(a, b) {
	if(!a || !b) return null
	if(!a.length || !b.length) return null

	var current, hours = [], i = 0, j = 0

	while(i < a.length && j < b.length) {
		if(compareHours(a[i].open, b[j].open) == -1) {
			current = createHours(b[j], a[i])
			if(current) {
				if(current.close == a[i].close) i++
				if(current.close == b[j].close) j++
				hours.push(current)
			} else {
				i++
			}
		} else if(compareHours(a[i].open, b[j].open) == 1) {
			current = createHours(a[i], b[j])
			if(current) {
				if(current.close == a[i].close) i++
				if(current.close == b[j].close) j++
				hours.push(current)
			} else {
				j++
			}
		} else {
			current = createHours(a[i], b[j])
			if(current.close == a[i].close) i++
			if(current.close == b[j].close) j++
			hours.push(current)
		}
	}
	
	return hours
}

function createHours(a, b) {
	var hours 
	if(compareHours(a.open, b.close) == -1) {
		hours = { day:a.day, open:a.open, open_string:a.open_string }
		if(compareHours(b.close, a.close) == -1) {
			hours.close = b.close
			hours.close_string = b.close_string
		} else {
			hours.close = a.close
			hours.close_string = a.close_string
		}
	} 
	return hours
}

function compareHours(a, b) {
	if(a.hours < b.hours) return -1
	if(a.hours > b.hours) return 1
	if(a.minutes < b.minutes) return -1
	if(a.minutes > b.minutes) return 1
	return 0
}

function getHours(text) {
	var full_text = text
	  , hours_regex = /((?:Mon|Tues|Wednes|Thurs|Fri|Satur|Sun)day)\s*(\d+\:\d{2}\s*(?:am|pm)?)\s*.\s*(\d+\:\d{2}\s*(?:am|pm)?)/gi
	  , days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']
	  , hours = []
	  , match
	while(match = hours_regex.exec(full_text)) {
		hours[days.indexOf(match[1].toLowerCase())] = hours[days.indexOf(match[1].toLowerCase())] || [] 
		hours[days.indexOf(match[1].toLowerCase())].push({
			  day: match[1]
			, open: getHoursMinutes(match[2])
			, open_string: match[2]
			, close: getHoursMinutes(match[3])
			, close_string: match[3]
		})
	}
	return hours
}

function getHoursMinutes(time) {
	var parts = /(\d+)\:(\d{2})\s*(am|pm)?/i.exec(time)
	  , hours = (parseInt(parts[1]) + (!parts[3] || parts[3].toLowerCase() == 'am' ? 0 : 12)) % 24
	  , minutes = parseInt(parts[2])

	if(parts[3] && parts[3].toLowerCase() == 'am' && parseInt(parts[1]) == 12) {
		hours = 0
	}

	if(parts[3] && parts[3].toLowerCase() == 'pm' && parseInt(parts[1]) == 12) {
		hours = 12
	}

	return { hours:hours, minutes:minutes }
}