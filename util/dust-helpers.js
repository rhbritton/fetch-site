var currency = require('./currency')
  , routes = require('./routes')

module.exports = function(dust) {

	dust.helpers = require('dustjs-helpers').helpers

	dust.helpers.formatCurrency = function(chunk, ctx, bodies, params) {
		var amount = dust.helpers.tap(params.amount, chunk, ctx)
		return chunk.write(currency.format(parseFloat(amount)))
	}

	dust.helpers.breakIT = function(chunk, ctx, bodies, params) {
		var hours = dust.helpers.tap(params.hours, chunk, ctx)
		return chunk.write(hours.replace(/\n/g, '<br/>'))
	}

	dust.helpers.last4 = function(chunk, ctx, bodies, params) {
		var string = dust.helpers.tap(params.string, chunk, ctx)
		return chunk.write(string.slice(-4))
	}

	dust.helpers.iter = function(chunk, ctx, bodies, params) {
		var obj = ctx.current();
		for (var k in obj) {
			chunk = chunk.render(bodies.block, ctx.push({key: k, value: obj[k]}));
		}
		return chunk;
	}

	dust.helpers.join = function(chunk, ctx, bodies, params) {
		var delimiter = params.delimiter ? dust.helpers.tap(params.delimiter, chunk, ctx) : ', '
		  , array = dust.helpers.tap(params.array, chunk, ctx)
		return chunk.write(array.join(delimiter))
	}

	dust.helpers.up = function(chunk, ctx, bodies, params) {
		var url = dust.helpers.tap(params.url, chunk, ctx)
		return chunk.write(routes.up(url))
	}

	dust.helpers.startsWith = function(chunk, ctx, bodies, params) {
		var needle = dust.helpers.tap(params.needle, chunk, ctx)
		  , haystack = dust.helpers.tap(params.haystack, chunk, ctx)

		if(haystack.indexOf(needle) == 0)
			return chunk.render(bodies.block, ctx)
		else if(bodies['else'])
			return chunk.render(bodies['else'], ctx)

		return chunk
	}

}