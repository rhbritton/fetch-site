var http = require('http')

module.exports = function(location, sensor, fn) {
	var options = getRequest(location, sensor)
	  , res = http.request(options, function(res) {
	  		var data = ''
	  		res.on('data', function(chunk) {
	  			data += chunk
	  		})
	  		res.on('end', function() {
	  			var json = JSON.parse(data)
	  			fn(null, json)
	  		})
	  })
	res.end()
}


function getRequest(location, sensor) {
	if (typeof location == 'object' && location['latitude'] != null && location['longitude'] != null) {
		// if location is an object with lat and long properties, we do reverse geocoding
		var path = '/maps/api/geocode/json?latlng=' + escape(location['latitude'].toString() + ',' + location['longitude'].toString()) + '&sensor=' + sensor;
	} else {
		// normal forward geocoding
		var path = '/maps/api/geocode/json?address=' + escape(location) + '&sensor=' + sensor;
	}

	return {
		host: 'maps.googleapis.com',
		port: 80,
		path: path,
		method: 'GET'
	};
}