exports.matches = function(a, b) {
	a = a.replace(/^\/|\/$/g, '')
	b = b.replace(/^\/|\/$/g, '')
	var parts = a.length ? a.split('/') : []
	  , matches = []
	  , current = ''

	parts.every(function(part) {
		var match = b.indexOf(current+part) == 0
		current = current+part+'/'
		return match && matches.push(part)
	})

	return matches
}

exports.up = function(url) {
	url = url.replace(/^\/|\/$/g, '')
	var url_parts = url.length ? url.split('/') : []

	url_parts.splice(-1)

	return '/' + url_parts.join('/')
}