var Restaurants = require('../models/restaurants')
  , Items = require('../models/items')
  , Values = require('../models/values')
  , Order = require( '../models/orders' )
  , geolib = require('geolib')

exports.hasRestaurant = function(order, restaurant_id) {
	for(var i = 0; i < order.restaurants.length; i++) {
		if(order.restaurants[i].restaurant == restaurant_id) 
			return order.restaurants[i]
	}
}

exports.calculate = function(order, location, drinks_id, fn) {
	var remaining_restaurants = 0
	  , error_response

	order.subtotal = 0
	order.items = 0
	order.tax = 0

	if(!order.restaurants.length) 
		return fn(error_response, order)

	order.restaurants.forEach(function(restaurant, r_index) {
		remaining_restaurants++
		restaurant.subtotal = 0
		Restaurants.get(restaurant.restaurant, function(err, r) {
			if(err || !Array.isArray(restaurant.items) || !restaurant.items.length) {
				order.restaurants.splice(r_index, 1)
				error_response = err || new Error('no items')
				return itemsDone()
			}

			var remaining_items = 0
			restaurant.items.forEach(function(item, i_index){
				remaining_items++
				
				Items.get(item.item, function(err, i) {
					if(err) {
						restaurant.items.splice(i_index, 1)
						error_response = err
						if(!--remaining_items) itemsDone()
						return
					}
			
					item.restaurant = r.id
					item.quantity = Math.min((Math.max(1, Math.floor(item.quantity)) || 1), 99)
					item.name = i.name
					item.subtotal = i.price

					Items.populateOptions(i, function(err, i) {
						if(err) {
							item.options = []
							error_response = err
							if(!--remaining_items) itemsDone()
							return
						}

						if(!Array.isArray(item.options)) item.options = []
			
						item.options.forEach(function(option, option_index) {
							if(!Array.isArray(option.values)) option.values = []
							
							var o = getEntityById(i.options, option.id)
							
							if(!o || (option.values.length == 1 && !option.values[0].id)) 
								return item.options.splice(option_index, 1)
							
							if(o.number) {
								option.values = option.values.slice(0, o.number)
							}
							
							option.values.forEach(function(value) {
								var v = getEntityById(o.values, value.id)
								item.subtotal += v.price || 0
							})
						})

						item.total = item.subtotal * item.quantity
						
						restaurant.subtotal += item.total
						order.subtotal += item.total
						order.items += item.quantity

						if(!--remaining_items) itemsDone()
					})
				})
			})
			function itemsDone() {
				restaurant.deliveryfee = exports.getFee(r, location)
				order.tax += Math.ceil(restaurant.subtotal*.103*100)/100
				if(!--remaining_restaurants) restaurantsDone()
			}
		})
	})

	function restaurantsDone() {
		var drinks = exports.hasRestaurant(order, drinks_id)
		
		if(drinks && order.restaurants.length > 1)
			drinks.deliveryfee = 0
		
		order.deliveryfee = calculateDeliveryTotal(order.restaurants)
		order.total = order.subtotal+order.tax+order.deliveryfee
		fn(error_response, order)
	}
}

exports.getFee = function(restaurant, location) {
	var fee = restaurant.deliveryfee
	  , price_per_mile = 0.9
	  , base_price = 5.99

	var distance = metersToMiles(geolib.getDistance({ latitude:restaurant.lat, longitude:restaurant.lng }, location))

	if(distance <= 5) return base_price

	return base_price + (Math.ceil(distance)-5)*price_per_mile
}

function getEntityById(entity, id) {
	for(var i = 0; i < entity.length; i++) {
		if(entity[i].id == id) return entity[i]
	}
}
function calculateDeliveryTotal( restaurants ) {

	var max = restaurants[0].deliveryfee || 0
	  , total = max
	  , fee
	
	for( var i = 1; i < restaurants.length; i++ ) {
		fee = restaurants[i].deliveryfee

		if(fee > max) max = fee

		total += fee || 0
	}

	return max + (total - max) / 2
}

function metersToMiles(meters) {
	return meters/1609.34
}