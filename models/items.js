var Client = require('node-rest-client').Client
  , item = new Client()
  , api_url = require('../config.json').api_url
  , Option = require('./options')

item.registerMethod('fetch', api_url+'/items', 'GET')
item.registerMethod('get', api_url+'/items/${id}', 'GET')
item.registerMethod('options', api_url+'/items/${id}/options', 'GET')

exports.get = function(id, fn) {
	var args = {}
	  , method
	  , req
	if(!fn && typeof id == 'function') {
		fn = id
		method = 'fetch'
	} else {
		args = { path:{ id:id } }
		method = 'get'
	}
	
	req = item.methods[method](args, function(data, res) {
		if(res.statusCode < 200 || res.statusCode >= 300) {
			fn(data)
		} else {
			fn(null, data, res)
		}
	})

	req.on('error', fn)
}

exports.options = function(id, fn) {
	var req = item.methods.options({ path:{ id:id } }, function(data, res) {
		if(res.statusCode < 200 || res.statusCode >= 300) {
			fn(data)
		} else {
			fn(null, data, res)
		}
	})

	req.on('error', fn)
}

exports.populateOptions = function(item, fn) {
	var error
	if(item && item.options && item.options.length) {
		exports.options(item.id, function(err, options) {
			if(err) return fn(err)
			if(!options.length) return fn(null, item)

			var remaining = 0
			options.forEach(function(option) {
				remaining++
				Option.populateValues(option, function(err, option) {
					if(err) error = err

					if(!--remaining) {
						item.options = options
						fn(error, item)
					}
				})
			})
		})
	} else {
		fn(null, item)
	}
}