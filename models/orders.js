var Client = require('node-rest-client').Client
  , order = new Client()
  , api_url = require('../config.json').api_url

order.registerMethod('fetch', api_url+'/orders', 'GET')
order.registerMethod('get', api_url+'/orders/${id}', 'GET')
order.registerMethod('create', api_url+'/orders', 'POST')
order.registerMethod('update', api_url+'/orders/${id}', 'PUT')

exports.get = function(id, fn) {
	var args = {}
	  , method
	  , req
	if(!fn && typeof id == 'function') {
		fn = id
		method = 'fetch'
	} else {
		args = { path:{ id:id } }
		method = 'get'
	}
	
	req = order.methods[method](args, function(data, res) {
		if(res.statusCode < 200 || res.statusCode >= 300) {
			fn(data)
		} else {
			fn(null, data, res)
		}
	})

	req.on('error', fn)
}

exports.create = function(data, fn) {
	data.datetime = new Date()

	var d2 = data

  order.methods.create({ data: data, headers:{ "Content-Type": "application/json" } }, function(data, res) {
    
    if(res.statusCode < 200 || res.statusCode >= 300) {
      console.log('error', data)
      fn(data)
    } else {
      fn(null, data, res)
    }
  })

}

exports.update = function(data, fn) {
	order.methods.update({ path:{ id:data.id }, data: data, headers:{ "Content-Type": "application/json" } }, function(data, res) {
		if(res.statusCode < 200 || res.statusCode >= 300) {
			fn(data)
		} else {
			fn(null, data, res)
		}
	})
}
