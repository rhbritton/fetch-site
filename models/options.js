var Client = require('node-rest-client').Client
  , option = new Client()
  , api_url = require('../config.json').api_url
  , Value = require('./values')

option.registerMethod('fetch', api_url+'/options', 'GET')
option.registerMethod('get', api_url+'/options/${id}', 'GET')
option.registerMethod('values', api_url+'/options/${id}/values', 'GET')

exports.get = function(id, fn) {
	var args = {}
	  , method
	  , req
	if(!fn && typeof id == 'function') {
		fn = id
		method = 'fetch'
	} else {
		args = { path:{ id:id } }
		method = 'get'
	}
	
	req = option.methods[method](args, function(data, res) {
		if(res.statusCode < 200 || res.statusCode >= 300) {
			fn(data)
		} else {
			fn(null, data, res)
		}
	})

	req.on('error', fn)
}

exports.values = function(id, fn) {
	var req = option.methods.values({ path:{ id:id } }, function(data, res) {
		if(res.statusCode < 200 || res.statusCode >= 300) {
			fn(data)
		} else {
			fn(null, data, res)
		}
	})

	req.on('error', fn)
}

exports.populateValues = function(option, fn) {
	if(option && option.values && option.values.length) {
		exports.values(option.id, function(err, values) {
			if(err) return fn(err)
			
			option.values = values
			fn(null, option)
		})
	} else {
		fn(null, option)
	}
}