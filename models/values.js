var Client = require('node-rest-client').Client
  , value = new Client()
  , api_url = require('../config.json').api_url

value.registerMethod('fetch', api_url+'/values', 'GET')
value.registerMethod('get', api_url+'/values/${id}', 'GET')

exports.get = function(id, fn) {
	var args = {}
	  , method
	  , req
	if(!fn && typeof id == 'function') {
		fn = id
		method = 'fetch'
	} else {
		args = { path:{ id:id } }
		method = 'get'
	}
	
	req = value.methods[method](args, function(data, res) {
		if(res.statusCode < 200 || res.statusCode >= 300) {
			fn(data)
		} else {
			fn(null, data, res)
		}
	})

	req.on('error', fn)
}