var Client = require('node-rest-client').Client
  , item = new Client()
  , api_url = require('../config.json').api_url

item.registerMethod('fetch', api_url+'/fetch', 'GET')

exports.get = function(fn) {
	var args = {}
	  , method
	  , req
	
	req = item.methods.fetch(args, function(data, res) {
		if(res.statusCode < 200 || res.statusCode >= 300) {
			fn(data)
		} else {
			fn(null, data, res)
		}
	})

	req.on('error', fn)
}