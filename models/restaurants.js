var Client = require('node-rest-client').Client
  , restaurant = new Client()
  , api_url = require('../config.json').api_url

restaurant.registerMethod('fetch', api_url+'/restaurants', 'GET')
restaurant.registerMethod('get', api_url+'/restaurants/${id}', 'GET')
restaurant.registerMethod('categories', api_url+'/restaurants/${id}/categories', 'GET')
restaurant.registerMethod('getRestaurantsInRadius', api_url+'/restaurants/radius', 'GET')

exports.get = function(id, fn) {
	var args = {}
	  , method
	  , req
	if(!fn && typeof id == 'function') {
		fn = id
		method = 'fetch'
	} else {
		args = { path:{ id:id } }
		method = 'get'
	}
	
	req = restaurant.methods[method](args, function(data, res) {
		if(res.statusCode < 200 || res.statusCode >= 300) {
			fn(data)
		} else {
			fn(null, data, res)
		}
	})

	req.on('error', fn)
}

exports.categories = function(id, fn) {
	var req = restaurant.methods.categories({ path:{ id:id } }, function(data, res) {
		if(res.statusCode < 200 || res.statusCode >= 300) {
			fn(data)
		} else {
			fn(null, data, res)
		}
	})

	req.on('error', fn)
}

exports.getInRadius = function(point, radius, fn) {
	var lat = parseFloat(point.lat || point[0])
	  , lng = parseFloat(point.lng || point[1])
	req = restaurant.methods['getRestaurantsInRadius']({ path:{ lat:lat, lng:lng, radius:radius } }, function(data, res) {
		if(res.statusCode < 200 || res.statusCode >= 300) fn(data)
		else fn(null, data, res)
	})

	req.on('error', fn)

}