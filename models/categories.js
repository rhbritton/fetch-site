var Client = require('node-rest-client').Client
  , category = new Client()
  , api_url = require('../config.json').api_url

category.registerMethod('fetch', api_url+'/categories', 'GET')
category.registerMethod('get', api_url+'/categories/${id}', 'GET')
category.registerMethod('items', api_url+'/categories/${id}/items', 'GET')

exports.get = function(id, fn) {
	var args = {}
	  , method
	  , req
	if(!fn && typeof id == 'function') {
		fn = id
		method = 'fetch'
	} else {
		args = { path:{ id:id } }
		method = 'get'
	}
	
	req = category.methods[method](args, function(data, res) {
		if(res.statusCode < 200 || res.statusCode >= 300) {
			fn(data)
		} else {
			fn(null, data, res)
		}
	})

	req.on('error', fn)
}

exports.items = function(id, fn) {
	var req = category.methods.items({ path:{ id:id } }, function(data, res) {
		if(res.statusCode < 200 || res.statusCode >= 300) {
			fn(data)
		} else {
			fn(null, data, res)
		}
	})

	req.on('error', fn)
}