var consolidate = require('consolidate')
  , express = require('express')
  , app = module.exports = express()
  , mandrill = require('node-mandrill')('tZsayUYR9MYZdHDXQ3UA3w')
  , Restaurant = require('../models/restaurants')
  , Category = require('../models/categories')
  , Item = require('../models/items')
  , Fetch = require('../models/fetch')
  , Hours = require('../util/hours')
  , order = require('../util/order')
  , Config = require('../config')

app.set('views', process.cwd()+'/templates')
app.set('view engine', 'dust')
app.set('view cache', false)
app.engine('dust', consolidate.dust)

app.get('/', function(req, res, next) {
	Fetch.get(function(err, fetch) {
		if(err) return next(err)
		if(!fetch) return next(new Error('not found'))

		var hours = fetch.hours

		if(req.xhr)
			res.renderStack(['home', 'layout-content'], { hours: Hours(hours) })
		else
			res.renderStack(['home', 'layout-content', 'layout'], { hours: Hours(hours) })
	})
})

app.get('/restaurants', function(req, res, next) {
	Fetch.get(function(err, fetch) {
		if(err) return next(err)
		if(!fetch) return next(new Error('not found'))
		
		Restaurant.getInRadius([0,0], 11300, function(err, restaurants) {
			if(err) return next(err)
			if(!restaurants) return next(new Error('not found'))

			var location
		      , free_restaurants = 0

			if(req.session.address && req.session.address.loc) {
				var parts = req.session.address.loc.split(',')
				location = { latitude:parseFloat(parts[0])||0, longitude:parseFloat(parts[1])||0 }
			} else {
				location = { latitude:0, longitude:0 }
			}

			if(restaurants) {
				restaurants.forEach(function(restaurant, i) {
					restaurants[i].parsed_hours = Hours(restaurant.hours, fetch.hours)
					restaurants[i].delivery_fee = order.getFee(restaurant, location)
					if(restaurants[i].delivery_fee == 0) free_restaurants++
				})
			}

			restaurants.sort(function(a, b) {
				if(a.parsed_hours.open != b.parsed_hours.open) {
					return a.parsed_hours.open ? -1 : 1
				}
				if(a.delivery_fee != b.delivery_fee) {
					return a.delivery_fee-b.delivery_fee
				}
				return a.name < b.name ? -1 : 1
			})

			if(req.xhr)
				res.renderStack(['restaurants', 'layout-content'], { free_restaurants:free_restaurants, restaurants:restaurants })
			else
				res.renderStack(['restaurants', 'layout-content', 'layout'], { free_restaurants:free_restaurants, restaurants:restaurants })
		})
	})
})

app.get('/restaurants/:id', function(req, res, next) {
	Fetch.get(function(err, fetch) {
		if(err) return next(err)
		if(!fetch) return next(new Error('not found'))

		Restaurant.get(req.params['id'], function(err, restaurant) {
			if(err) return next(err)
			if(!restaurant) return next(new Error('not found'))

			restaurant.parsed_hours = Hours(restaurant.hours, fetch.hours)
			Restaurant.categories(req.params['id'], function(err, categories) {
				if(err) return next(err)
				if(!categories) return next(new Error('not found'))

				if(req.xhr)
					res.renderStack(['restaurant', 'layout-content'], { restaurant:restaurant, categories:categories })
				else
					res.renderStack(['restaurant', 'layout-content', 'layout'], { restaurant:restaurant, categories:categories })
			})
		})
	})
})

app.get('/restaurants/:id/:cid', function(req, res, next) {
	Restaurant.get(req.params['id'], function(err, restaurant) {
		if(err) return next(err)
		if(!restaurant) return next(new Error('not found'))

		Restaurant.categories(req.params['id'], function(err, categories) {
			if(err) return next(err)
			if(!categories) return next(new Error('not found'))

			Category.get(req.params['cid'], function(err, category) {
				if(err) return next(err)
				if(!category) return next(new Error('not found'))

				Category.items(req.params['cid'], function(err, items) {
					if(err) return next(err)
					if(!items) return next(new Error('not found'))

					if(categories) {
						categories.forEach(function(c, i) {
							if(c.id == category.id) {
								c.selected = true
								if(i > 0) categories[i-1].prev = true
								if(i < categories.length-1) categories[i+1].next = true
							}
						})
					}
					if(req.xhr)
						res.renderStack(['category', 'layout-content'], { restaurant:restaurant, categories:categories, category:category, items:items })
					else
						res.renderStack(['category', 'layout-content', 'layout'], { restaurant:restaurant, categories:categories, category:category, items:items })
				})
			})
		})
	})
})

app.get('/restaurants/:id/:cid/:iid', function(req, res, next) {
	Fetch.get(function(err, fetch) {
		if(err) return next(err)
		if(!fetch) return next(new Error('not found'))

		Restaurant.get(req.params['id'], function(err, restaurant) {
			if(err) return next(err)
			if(!restaurant) return next(new Error('not found'))

			restaurant.parsed_hours = Hours(restaurant.hours, fetch.hours)
			Category.get(req.params['cid'], function(err, category) {
				if(err) return next(err)
				if(!category) return next(new Error('not found'))

				Item.get(req.params['iid'], function(err, item) {
					if(err) return next(err)
					if(!item) return next(new Error('not found'))

					Item.populateOptions(item, function(err, populated_item) {
						if(err) return next(err)
						if(!populated_item) return next(new Error('not found'))

						if(req.xhr)
							res.renderStack(['item', 'layout-content'], { restaurant:restaurant, category:category, item:item })
						else
							res.renderStack(['item', 'layout-content', 'layout'], { restaurant:restaurant, category:category, item:item })
					})
				})
			})
		})
	})
})

app.get('/how-it-works', function(req, res, next) {
	if(req.xhr)
		res.renderStack(['how-it-works', 'layout-content'], {})
	else
		res.renderStack(['how-it-works', 'layout-content', 'layout'], {})
})

app.get('/contact', function(req, res, next) {
	Fetch.get(function(err, fetch) {
		if(err) return next(err)
		if(!fetch) return next(new Error('not found'))

		var hours
		if(fetch && fetch.hours) {
			hours = fetch.hours
			hours = hours.replace('\n', '<br/>')
		}

		if(req.xhr)
			res.renderStack(['contact', 'layout-content'], { hours: hours })
		else
			res.renderStack(['contact', 'layout-content', 'layout'], { hours: hours })
	})
})

app.post('/contact', function(req, res, next) {
	res.render('email/contact', { comment:req.body.comment }, function(err, html) {
		if(err) return next(err)
		if(!html) return next(new Error('not found'))

		mandrill('/messages/send', {
		    message: {
		        to: [{ email: Config.email, name: req.body.name }],
		        from_email: req.body.email,
		        subject: 'Fetch Website Contact Form',
		        html: html,
		        track_clicks: false
		    }
		}, function(err, response) {
			if(err) return next(err)

		    if(req.xhr)
				res.renderStack(['contact', 'layout-content'], { confirm:true })
			else
				res.renderStack(['contact', 'layout-content', 'layout'], { confirm:true })
		})
	})
})