var consolidate = require('consolidate')
  , express = require('express')
  , app = module.exports = express()
  , geolib = require('geolib')
  , Restaurant = require('../models/restaurants')

app.set('views', process.cwd()+'/templates')
app.set('view engine', 'dust')
app.set('view cache', false)
app.engine('dust', consolidate.dust)

app.post('/address', function(req, res, next) {
	var point = req.body.loc.split(',')
	  , roanoke = { latitude:37.27097, longitude:-79.941427 }
	  , address = { latitude:parseFloat(point[0]), longitude:parseFloat(point[1]) }
	  , valid = geolib.isPointInCircle(address, roanoke, 19312)

	Restaurant.getInRadius(point, 19312, function(err, data) {
		res.end((valid && data ? data.length : 0).toString())
	})
})