var consolidate = require('consolidate')
  , express = require('express')
  , balanced = require('balanced-official')
  , Order = require( '../models/orders' )
  , app = module.exports = express()
  , validator = require('validator')
  , order_util = require('../util/order')

app.set('views', process.cwd()+'/templates')
app.set('view engine', 'dust')
app.set('view cache', false)
app.engine('dust', consolidate.dust)

balanced.configure('ak-prod-iu6d4GPqrHpJ1xqnPD2yaqIZTuqBXiuJ')

app.get('/checkout(/guest)?', function(req, res, next) {
	var location
      , drinks_id = '53e91727e4b035614ac4c374'

	if(req.session.address && req.session.address.loc) {
		var parts = req.session.address.loc.split(',')
		location = { latitude:parseFloat(parts[0])||0, longitude:parseFloat(parts[1])||0 }
	} else {
		location = { latitude:0, longitude:0 }
	}

	order_util.calculate(req.session.order, location, drinks_id, function(err, order) {
		if(err) console.log('that was bad')

		req.session.account = req.session.account || {}
		req.session.order_approved = order

		if(!order.items) return res.redirect('/')
		
		if(req.query.drinks != 'false' && !order_util.hasRestaurant(order, drinks_id)) {
			if(req.xhr)
				return res.renderStack(['add-drinks', 'layout-content'], { drinks_id:drinks_id })
			else
				return res.renderStack(['add-drinks', 'layout-content', 'layout'], { drinks_id:drinks_id })
		}
		
		if(req.xhr)
			return res.renderStack(['checkout-confirm', 'layout-content'], {})
		else
			return res.renderStack(['checkout-confirm', 'layout-content', 'layout'], {})
	})
})

app.post('/checkout', function(req, res, next) {
	if(!validator.isEmail(req.body.email) || !req.body.phone || !req.body.name) {
		if(req.xhr)
			return res.renderStack(['checkout-confirm', 'layout-content'], { body: req.body })
		else
			return res.renderStack(['checkout-confirm', 'layout-content', 'layout'], { body: req.body })
	}

	var tip = Math.max(0, parseFloat(req.body.tip)) || 0
	  , total_cents = Math.round(100*(req.session.order_approved.total + tip))
	  , card_href = req.body.cc_href
	
	if(!req.session.logged_in) {
		req.session.account = req.session.account || {}
		req.session.account.name = req.body.name
		req.session.account.phone = req.body.phone.replace(/[^\d+!x]/gi, '')
				.replace(/(\d*)(\d{3})(\d{3})(\d{4})/, '$1-$2-$3-$4')
				.replace(/^(\+)?-/, '$1')
				.replace(/x/, ' x')
		req.session.account.email = req.body.email
		req.session.account.address = res.locals.address
	}

	if(req.body.type == 'card') {
		balanced.get(card_href).debit({
			  appears_on_statement_as: 'Fetch Delivery'
			, amount: total_cents
			, description: 'Order from: ' + req.body.name + '('+req.body.email+')'
		}).then(function(data) {
			completeOrder(data)
		}, function(err) {
			if(req.xhr)
				return res.renderStack(['checkout-confirm', 'layout-content'], { card_err: true, body: req.body })
			else
				return res.renderStack(['checkout-confirm', 'layout-content', 'layout'], { card_err: true, body: req.body })
		})
	} else {
		completeOrder()
	}

	function completeOrder(data) {
		var order = req.session.order_approved
		order.account = req.session.account
		order.address = res.locals.address
		order.address.delivery_instructions = req.body.delivery_instructions
		order.payment = { type:req.body.type }
		order.tip = tip
		order.total += tip
		
		if(data) {
			order.payment.transaction = data.transaction_number
			order.payment.debit = data.href
			order.payment.card = card_href
		}

		Order.create( order, function( err, data ) {
			//TODO: if there's an error we need to refund their card
			//if(err) return next(err)

			if(req.xhr) {
				
			}
		})

		delete req.session.order
		res.cookie('order', '', { expires: new Date(1), path: '/' })
		
		if(req.xhr)
			return res.renderStack(['checkout-complete', 'layout-content'], { order:order })
		else
			return res.renderStack(['checkout-complete', 'layout-content', 'layout'], { order:order })
	}

})