#!/bin/sh
deploy_host="fetch.gamegu.in"

cd ..
tar czf fetch-site.tar.gz fetch-site
cd fetch-site
scp -i /Users/elchupa/.ssh/fetchBooks.pem ../fetch-site.tar.gz ubuntu@"$deploy_host":fetch-api-0.0.1.jar
scp -i /Users/elchupa/.ssh/fetchBooks.pem ./supervisord.conf ubuntu@"$deploy_host":fetch.conf
ssh -i /Users/elchupa/.ssh/fetchBooks.pem ubuntu@"$deploy_host" sudo mv /home/ubuntu/fetch.conf /etc/supervisor/conf.d/fetch_frontend.conf
ssh -i /Users/elchupa/.ssh/fetchBooks.pem ubuntu@"$deploy_host" sudo supervisorctl stop fetch-frontend
ssh -i /Users/elchupa/.ssh/fetchBooks.pem ubuntu@"$deploy_host" sudo supervisorctl reread
ssh -i /Users/elchupa/.ssh/fetchBooks.pem ubuntu@"$deploy_host" sudo supervisorctl update
ssh -i /Users/elchupa/.ssh/fetchBooks.pem ubuntu@"$deploy_host" sudo supervisorctl start fetch-frontend
