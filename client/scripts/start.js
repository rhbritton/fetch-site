require('./lib/jquery.replaceanim')
require('./polyfills/overflow-detect')
require('./polyfills/raf')
require('../../util/dust-helpers')(require('../../build/dust-runtime'))

var loader = require('./loader')
  , modules = require('./modules')

modules.init($(document.body), true)

loader.on('load', function($container) {
	modules.init($container)
})