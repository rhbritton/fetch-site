;(function($) {

	$.fn.replaceWithAnim = function($replacement) {
		var $original = this.after($replacement.css({ opacity:0.01 }))
		  , border_box = $replacement.css('box-sizing') == 'border-box'
		  , original_width = $original[border_box ? 'outerWidth' : 'width']()
		  , original_height = $original[border_box ? 'outerHeight' : 'height']()
		  , original_offset = $original.offset()
		  , matches
		  , done

		$replacement.css({
			  position: 'fixed'
			, top: original_offset.top
			, left: original_offset.left
			, width: original_width
			, height: original_height
		})

		matches = getMatches($original, $replacement)

		setTimeout(function() {
			for(var i = 0; i < matches.length; i++) {
				animate(matches[i])
			}
		}, 0)
			
		$replacement.velocity({ opacity:1 }, { duration:300, complete:function() {
			setTimeout(function() {
				$replacement.css({ 
					position: '', 
					top: '', 
					left: '', 
					width:'', 
					height:'',
					opacity:''
				})
			}, 100)
			$original.remove()
		} })
	}
	
	function getMatches($original, $replacement) {
		var matches = []
		$original.find('[id]:visible').each(function() {
			var $o = $(this)
			  , $r = $replacement.find('#'+$o.attr('id')).filter(':visible').first()

			if($r.length) matches.push({ $original:$o, $replacement:$r })
		})
		return matches
	}

	function animate(match) {
		var $clone = match.$original.clone().appendTo(document.body)
		  , border_box = $clone.css('box-sizing') == 'border-box'
		  , original_width = match.$original[border_box ? 'outerWidth' : 'width']()
		  , original_height = match.$original[border_box ? 'outerHeight' : 'height']()
		  , replacement_width = match.$replacement[border_box ? 'outerWidth' : 'width']()
		  , replacement_height = match.$replacement[border_box ? 'outerHeight' : 'height']()
		  , original_offset = match.$original.offset()
		  , replacement_offset = match.$replacement.offset()
		  , target

		target = {
	  		  top: [replacement_offset.top, original_offset.top]
	  		, left: [replacement_offset.left, original_offset.left]
			, width: [replacement_width, original_width]
			, height: [replacement_height, original_height]
		}

		$clone.css({
			  position: 'fixed'
			, top: original_offset.top
			, left: original_offset.left
			, width: original_width
			, height: original_height
		})

		$clone.velocity(target, { duration:400, easing:[.4,0,.2,1], complete:function() {
			match.$replacement.css({ visibility:'' })
			$clone.remove()
		} })

		match.$original.css({ visibility:'hidden' })
		match.$replacement.css({ visibility:'hidden' })
		match.$clone = $clone
	}

})(jQuery);

