var EventEmitter = require('events').EventEmitter
  , routes = require('../../util/routes')
  , Throttle = require('../../util/throttle')
  , $body = $(document.body)
  , loader = module.exports = new EventEmitter()
  , indicator_timeout
  , current_url = window.location.pathname
  , current
  , cache = {}

loader.unload = function(url) {
	delete cache[url]
}

loader.preload = function(url, force, fn) {
	if(!Modernizr.history) return
	if(cache[url] && !cache[url].failed && !force) return

	var prev = current_url

	cache[url] = cache[url] || {
		throttle: new Throttle(250)
	}

	cache[url].loading = true
	cache[url].time = Date.now()

	cache[url].throttle.exec(function(done) {
		$.get(url+(url.indexOf('?')+1 ? '&' : '?')+'ts='+Date.now(), function(html, something, xhr) {
			if(cache[url]) {
				cache[url].html = html
				cache[url].failed = false
				cache[url].loading = false
				if(cache[url].insert && !cache[url].throttle.timeout) {
					cache[url].insert = false
					loader.insert(html, url, prev)
				}
			}
			done()
			fn && fn()
		}).fail(function(xhr) {
			cache[url].failed = true
			if(cache[url].insert && !cache[url].throttle.timeout) {
				var $container = loader.getContainer(url, prev)
		  		loader.emit('fail', $container, url, prev, xhr)
				cache[url].insert = false
		  	}
		})
	})
}

loader.load = function(url, nopush) {
	if(!Modernizr.history) return window.location.href = url

	if(current_url == url && (!cache[url] || current > cache[url].time)) {
		current = Date.now()
		loader.emit('static', url)
		return
	}

	var prev = current_url
	  , $scroller = $body.find('.content[role=main]')

	loader.emit('start', url, prev, nopush)
	
	if(!nopush) {
		history.replaceState({ scrollTop:$scroller.scrollTop() }, '', prev)
		history.pushState({ scrollTop:0 }, '', url)
	}

	if(cache[url] && !cache[url].failed) {
		if(cache[url].loading) cache[url].insert = true
		else loader.insert(cache[url].html, url, prev)
	} else {
		loader.preload(url)
		cache[url].insert = true
	}

	current_url = url
}

loader.submit = function($form) {
	var url = $form.attr('action')
	  , prev = current_url

	loader.emit('start', url, prev)
	history.replaceState({ scrollTop:0 }, '', url)
	current_url = url
	$.post(url, $form.serialize(), function(html) {
		loader.insert(html, url, prev)
	})
}

loader.insert = function(html, url, prev) {
	var $container = loader.getContainer(url, prev)
	  , $new_container = $(html)

	$container.replaceWithAnim($new_container)
	if(history.state) $new_container.scrollTop(history.state.scrollTop)
	loader.hijack($new_container)

	$body.attr('data-path', url)

	loader.emit('load', $new_container, url, prev)
}

loader.hijack = function($el) {
	var $links = $el.find('a:not([data-prevent-load])[href^="/"]')
	  , $forms = $el.find('form:not([data-prevent-submit])[action^="/"]')

	current = Date.now()

	if(Modernizr.history) {
		setTimeout(function() {
			var preload = []
			$links.click(function(e) {
				var href = $(this).attr('href')
				if(!e.isDefaultPrevented()) {
					if($(this).attr('data-replace-state')) {
						history.replaceState({}, '', href)
						loader.load(href, true)
					} else {
						loader.load(href)
					}
				}
				e.preventDefault()
			}).each(function() {
				var href = $(this).attr('href')
				if(preload.indexOf(href) == -1) preload.push(href)
			})

			var i = 0
			;(function iterate() {
				if(i == preload.length) return
				loader.preload(preload[i++], false, iterate)
			})()

			$forms.submit(function(e) {
				if(!e.isDefaultPrevented()) {
					loader.submit($(this))
				}
				e.preventDefault()
			})
			
			$links.on('mouseover', preloadLink)
			$links.on('touchstart', preloadLink)

		}, 100)

		function preloadLink(e) {
			var href = $(this).attr('href')
			loader.preload(href)
		}
	}
}

loader.getContainer = function(url, prev) {
	var matches = routes.matches(prev, url)
	  , $container
	  , path

	for(var i = matches.length; i >= 0; i--) {
		path = '/'+matches.join('/')
		$container = $('[data-container="'+path+'"]')
		if($container.size()) break
		else matches.splice(-1, 1)
	}

	return $container || $(document.body)
}

loader.hijack($body)

loader.on('load', function(container, url, prev) {
	loader.preload(prev)
})

$(window).on('popstate', function(e) {
	if(e.originalEvent.state) {
		loader.load(window.location.pathname, true)
	}
})