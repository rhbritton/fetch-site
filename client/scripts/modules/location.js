var modules = require('./')
  , loader = require('../loader')
  , jsdiff = require('diff')

var Location = function($el) {
	var location = this

	this.$el = $el
	this.$content = this.$el.find('.content')
	this.$set = this.$el.find('a.set')
	this.$input = this.$el.find('input')
	this.$street = this.$input.filter('[name=street]')
	this.$street_suggest = this.$street.siblings('[name=autocomplete]')
	this.$city = this.$input.filter('[name=city]')
	this.$state = this.$el.find('[name=state]')
	this.$zip = this.$input.filter('[name=zip]')
	
	this.$input.on('keyup', this.handleKeypress.bind(this))
	this.$state.on('change', this.search.bind(this))
	this.$street.on('blur', this.streetBlur.bind(this))
	this.$el.find('.close').click(this.close.bind(this))

	this.$street_suggest.on('focus click', this.removeAutoComplete.bind(this))

	this.geocoder = new google.maps.Geocoder()
	this.map = new google.maps.Map(this.$el.find('.map')[0], { disableDefaultUI: true })
	this.places = new google.maps.places.PlacesService(this.map)
	this.marker = new google.maps.Marker()

	$('a[href^="/restaurants"]').click(this.getRestaurants.bind(this))

	loader.on('load', function($content, url, prev, pop) {
		$content.find('a[href^="/restaurants"]').click(location.getRestaurants.bind(location))
	})

	this.detectLocation()
}

Location.prototype.removeAutoComplete = function() {
	this.$street.focus()
	this.$street_suggest.hide()
}

Location.prototype.detectLocation = function() {
	var location = this

	if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var coords = position.coords
			detected_location = new google.maps.LatLng(coords.latitude, coords.longitude)
			location.map.setCenter(detected_location)
			location.geocoder.geocode({ latLng:detected_location }, function(results, status) {
				var data = location.getValidAddress(results)
				  , address = data && getAddressComponents(data)

				if(data) {
					location.$city.val(address.city)
					location.$state.val(address.state)
					location.$zip.val(address.zip)
					
					if(coords.accuracy < 50) {
						location.map.setZoom(17)
						location.$street.val(address.street)
						location.setAddress(data)
					} else {
						location.$street.val('').focus()
						location.$street_suggest.val('')
						location.map.setZoom(10)
					}
				} else {
					location.map.setZoom(10)
				}

				if(!location.$street.val() || !location.$city.val() || !location.$state.val() || !location.$zip.val()) {
					location.showError('Enter Address', true)
				}
			})

		})
	}
}

Location.prototype.handleKeypress = function(e) {
	if(this.$street_suggest.val().indexOf(this.$street.val()) != 0) {
		this.$street_suggest.val('')
	}

	if(e.originalEvent.which == 13 && this.valid_address) {
		this.$set.click()
	} else {
		clearTimeout(this.search_timeout)
		this.search_timeout = setTimeout(this.search.bind(this), 400)
	}
}

Location.prototype.streetBlur = function() {
	if(this.$street_suggest.val()) {
		this.$street.val(this.$street_suggest.val())
	}
}

Location.prototype.search = function() {
	if(!this.$street.val() || !this.$city.val() || !this.$state.val() || !this.$zip.val()) {
		this.showError('Enter Address', true)
	} else if(this.$street.val().match(/^\d+/)) {
		this.searchAddress()
	} else {
		this.searchPlace()
	}
}

Location.prototype.searchAddress = function() {
	var address = [this.$street.val(), this.$city.val()+',', this.$state.val(), this.$zip.val()].join(' ')
	this.setLocation({ address:address })
}

Location.prototype.searchPlace = function() {
	var location = this
	  , city = location.$city.val()+', '+location.$state.val()
	location.geocoder.geocode({ address:city }, function(results, status) {
		if(results && results[0] && results[0].geometry && results[0].geometry.location) {
			location.places.nearbySearch({ location:results[0].geometry.location, radius:25000, name:location.$street.val() }, function(results, status) {
				var place = results[0]
				if(!place) return location.searchAddress()
				location.setLocation({ address:place.vicinity }, place, function(address) {
					if(!address) location.setLocation({ latLng:place.geometry.location }, place)
				})
			})
		}
	})
}

Location.prototype.setLocation = function(loc, place, fn) {
	var location = this
	location.geocoder.geocode(loc, function(results, status) {
		var address = location.getValidAddress(results)
		if(address) {
			if(place) {
				address.geometry = place.geometry
				address.name = place.name
			}
			location.setAddress(address)
		}
		fn && fn(address, results)
	})
}

Location.prototype.setAddress = function(address) {
	var location = this

	if(!address) {
		location.map.setZoom(13)
		location.marker.setMap(null)
		return
	}

	location.map.setZoom(17)
	location.map.setCenter(address.geometry.location)

	var address_obj = getAddressComponents(address)
	  , match = matchPartitalAddress(location.$street.val(), address.name || address_obj.street, address_obj)

	if(match) {
		location.$street_suggest.val(match.suggest)
		
		$.post('/address', address_obj, function(data) {
			if(parseInt(data)) {
				location.valid_address = address_obj
				location.marker.setMap(location.map)
				location.marker.setPosition(address.geometry.location)
				
				location.$set.removeClass('error').addClass('valid')
				
				if(address.name) location.$set.html('<span class="name">'+address.name+'</span><span class="address">'+address_obj.street+'</span>')
				else location.$set.html(address_obj.street)
				
				if(location.$zip[0] != document.activeElement) location.$zip.val(address_obj.zip)
				if(location.$city[0] != document.activeElement) location.$city.val(address_obj.city)
			} else {
				location.showError('No Restaurants')
			}
		})
	} else {
		location.showError('Not Found')
	}
}

Location.prototype.getRestaurants = function(e) {
	if(this.$el.is(':visible')) {
		if(this.$set.hasClass('valid') && this.valid_address) {
			this.close()
			$.cookie('address', JSON.stringify(this.valid_address), { path: '/' })
			loader.preload('/restaurants', true)
			loader.preload('/checkout/guest', true)
			loader.preload('/checkout', true)
			loader.preload('/checkout/guest?drinks=false', true)
			loader.preload('/checkout?drinks=false', true)
			if(this.ignore_restaurant_link) e.preventDefault()
			if(this.fn) this.fn(this.valid_address)
		} else {
			e.preventDefault()
		}
	} else if(!$.cookie('address')) {
		e.preventDefault()
		this.open()
	}
}

Location.prototype.showError = function(msg, no_error) {
	this.valid_address = null
	this.marker.setMap(null)
	this.$set.removeClass('valid').addClass('error').html(msg)
	if(no_error) this.$set.removeClass('error')
	this.$street_suggest.val('')
}

Location.prototype.getValidAddress = function(results) {
	if(!results) return this.showError('Not Found')

	var valid

	for(var i = 0; i < results.length; i++) {
		if(isSpecificEnough(results[i])) {
			valid = results[i]
			if(valid.geometry.location_type == 'ROOFTOP') return valid
		}
	}

	return valid || this.showError('Not Specific')
}

Location.prototype.open = function(ignore_restaurant_link, fn) {
	var location = this
	this.ignore_restaurant_link = ignore_restaurant_link
	this.fn = fn

	this.detectLocation()

	this.$el.velocity('slideDown', { complete:function() {
		google.maps.event.trigger(location.map, 'resize')
	}})

}

Location.prototype.close = function() {
	this.$el.removeClass('super')
	this.$el.velocity('slideUp')
}

modules.register('location', Location)



function matchPartitalAddress(input, suggested) {
	if(!suggested) return
 
	var s1 = suggested
	  , s2 = expandStreetAbbreviations(suggested.replace('/\b()\b/'))
	  , sanitized_input = input.replace(/\s+/, ' ').toLowerCase()
	
	if(s1.toLowerCase().indexOf(sanitized_input) == 0) {
		return { partial:s1.substring(0, input.length), suggest:input+s1.substring(input.length), full:s1 }
	}

	if(s2.toLowerCase().indexOf(sanitized_input) == 0) {
		return { partial:s2.substring(0, input.length), suggest:input+s2.substring(input.length), full:s2 }
	}

	return matchDiffAddress(input, s1) || matchDiffAddress(input, s2)
}

function matchDiffAddress(input, suggested) {
	var diff = jsdiff.diffChars(input.toLowerCase(), suggested.toLowerCase())
	  , found = false
	  , count = 0 
	  , suggest = ''

	for(var i = diff.length-1; i >=0 ; i--) {
		if(!diff[i].added) found = true
		if(!found && diff[i].added) {
			suggest = diff[i].value + suggest
		} else if(diff[i].added || diff[i].removed) {
			count++
		}
	}

	if(suggested.match(/\d+/)) {
		if(count < 3 && input.match(/\d+/) && input.match(/\d+/)[0] == suggested.match(/\d+/)[0])
			return { partial:input, suggest:input+suggest, full:suggested }
	} else {
		return { partial:input, suggest:input+suggest, full:suggested }
	}

}

function expandStreetAbbreviations(address) {
	return address.replace(/\bSt\b/, 'Street')
	              .replace(/\bDr\b/, 'Drive')
	              .replace(/\bRd\b/, 'Road')
	              .replace(/\bBlvd\b/, 'Boulevard')
	              .replace(/\bPl\b/, 'Place')
	              .replace(/\bCt\b/, 'Court')
	              .replace(/\bCir\b/, 'Circle')
	              .replace(/\bHwy\b/, 'Highway')
	              .replace(/\bAve\b/, 'Avenue')
	              .replace(/\bLn\b/, 'Lane')
	              .replace(/\bN\b/, 'North')
	              .replace(/\bS\b/, 'South')
	              .replace(/\bE\b/, 'East')
	              .replace(/\bW\b/, 'West')
	              .replace(/\bNE\b/, 'Northeast')
	              .replace(/\bNW\b/, 'Northwest')
	              .replace(/\bSE\b/, 'Southeast')
	              .replace(/\bSW\b/, 'Southwest')
}

function formatAddress(address) {
	if(!address) return ''
	address = JSON.parse(address)
	return address.street + ' ' + address.city + ', ' + address.state
}

function isSpecificEnough(result) {
	var valid_types = ['street_address', 'premise', 'subpremise', 'natural_feature', 'point_of_interest', 'establishment', 'park', 'airport']
	return result && result.types && result.types.some(function(type) {
		return valid_types.indexOf(type) >= 0
	}) || false
}

function getAddressComponents(data) {
	var address = { loc: data.geometry.location.toUrlValue() }
	data.address_components.forEach(function(component) {
		if(component.types.indexOf('street_number') != -1) {
			address.street = component.short_name + ' '
		}
		if(component.types.indexOf('route') != -1) {
			address.street += component.short_name
		}
		if(component.types.indexOf('locality') != -1) {
			address.city = component.short_name
		}
		if(component.types.indexOf('administrative_area_level_1') != -1) {
			address.state = component.short_name
		}
		if(component.types.indexOf('postal_code') != -1) {
			address.zip = component.short_name
		}
		if(component.types.indexOf('country') != -1) {
			address.country = component.short_name
		}
	})
	return address
}