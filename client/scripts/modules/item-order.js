var modules = require('./')
  , currency = require('../../../util/currency')

modules.register('item-order', function($el) {
	var $selects = $el.find('select[name^=options]')
	  , $single_selects = $el.find('select[name^=options]:not([multiple])')
	  , $multi_selects = $el.find('select[name^=options][multiple]')
	  , base_price = parseFloat($el.attr('data-price'))
	  , $price_text = $el.find('h2.price')
	  , $price_field = $el.find('[name=subtotal]')
	  , $order = $('nav.order')
	  , adding = false
	  , $add_button = $el.find('button[type="submit"]')
	  , $quantity = $el.find('[name=quantity]')
	  , $instructions = $el.find('[name=instructions]')
	  , $popup = $('.order_popup')

	$selects.change(function() {
		var total = base_price
		$selects.find('option:selected').each(function() {
			total += parseFloat($(this).attr('data-price'))
		})
		$price_field.val(total)
		$price_text.text(currency.format(total))
	})

	$single_selects.change(function() {
		var $selected = $(this).find('option:selected')
		$(this).find('option').each(function() {
			if(this == $selected[0]) {
				$(this).text($(this).text().replace(/\[.*\]$/, '[Included]'))
			} else {
				var price_change = parseFloat($(this).attr('data-price')) - parseFloat($selected.attr('data-price'))
				$(this).text($(this).text().replace(/\[.*\]$/, '['+currency.format(price_change, { force_sign: true })+']'))
			}
		})
	})

	$multi_selects.chosen().change(function() {
		var $this = $(this)
		  , $selected
		  , max = parseInt($this.attr('data-max'))
		if(max) {
			$selected = $this.find(':selected')
			if($selected.size() > max) {
				this.setCustomValidity('You have selected more than '+max+' options.')
			} else {
				this.setCustomValidity('')
			}
		}
	})

	$quantity.blur(function() {
		$quantity.val(Math.min((Math.max(1, Math.floor($quantity.val())) || 1), 99))
	})

	$instructions.keyup(function() {
		if($instructions.val()) {
			$el.find('label.agreement').show().find('input').attr('required', true)
		} else {
			$el.find('label.agreement').hide().find('input').removeAttr('required')
		}
	})

	$el.submit(function(e) {
		e.preventDefault()
		$order.module().addItem($el)
		$el[0].reset()
		$selects.change()
		$popup.addClass('active')
		setTimeout(function() {
			$popup.removeClass('active')
		}, 2000)
	})
}) 