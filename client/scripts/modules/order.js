var Order
  , modules = require('./')
  , currency = require('../../../util/currency')
  , Throttle = require('../../../util/throttle')
  , loader = require('../loader')
  , qs = require('qs')
  , dust = require('../../../build/dust-runtime')

//templates
require('../../../build/templates/order-item')()
require('../../../build/templates/order-restaurant')()
  
Order = function($el) {
	var order = this
	  , cookie = JSON.parse($.cookie('order')||'{}')

	order.$el = $el
	order.toggle_quantity_element = $('header[role=main] a.order .items')
	order.subtotal_element = $el.find('.subtotal .amount')
	order.quantity_element = $el.find('.items .amount')
	
	order.subtotal = cookie.subtotal || 0
	order.quantity = cookie.items || 0
	order.restaurants = cookie.restaurants || []
	order.items = {}

	order.restaurants.forEach(function(r) {
		r.items.forEach(function(i) {
			order.items[i.id] = i
		})
	})

	order.$el.on('click', 'a.delete', this.deleteItem.bind(this))
	order.$el.on('change', 'input.quantity', this.submitQty.bind(this))
	order.$el.on('submit', 'form.quantity', this.updateQty.bind(this))
}

Order.prototype.store = function() {
	$.cookie('order', JSON.stringify({ items:this.quantity, subtotal:this.subtotal, restaurants:this.restaurants }), { path:'/' })
}

Order.prototype.getRestaurant = function(id, pull) {
	for(var i = 0; i < this.restaurants.length; i++) {
		if(this.restaurants[i].restaurant == id) {
			if(!pull) return this.restaurants[i]
			else return this.restaurants.splice(i, 1)[0]
		}
	}
}

Order.prototype.getItem = function(restaurant, item_id, pull) {
	var items = restaurant.items
	for(var i = 0; i < items.length; i++) {
		if(items[i].id == item_id) {
			if(!pull) return items[i]
			else return items.splice(i, 1)[0]
		}
	}
}

Order.prototype.addRestaurant = function(id, name) {
	var restaurant = { restaurant:id, name:name, items:[] }
	this.restaurants.push(restaurant)
	return restaurant
}

Order.prototype.addItem = function($item) {
	var item_data = qs.parse($item.serialize())
	  , restaurant = this.getRestaurant(item_data.restaurant) || this.addRestaurant(item_data.restaurant, $('.restaurant_info h2 .name').text())
	  , item = this.createItem($item, item_data)

	this.subtotal += item.total
	this.quantity += item.quantity

	restaurant.items.push(item)

	this.renderNewItem(restaurant, item)
	this.updateDOM()
	this.store()

	loader.preload('/checkout', true)
	loader.preload('/checkout?drinks=false', true)
	loader.preload('/checkout/guest', true)
	loader.preload('/checkout/guest?drinks=false', true)
}

Order.prototype.createItem = function($item, item_data) {
	var item = {}
	  , options = []

	item_data.options && Object.keys(item_data.options).forEach(function(id) {
		var $option = $item.find('[name="options['+id+']"]')
		  , option = { id:id, name:$option.prev('label').text(), values:[] }
		  , val = item_data.options[id]
		
		if(Array.isArray(val)) {
			val.forEach(function(val) {
				option.values.push({ id:val, name:$option.find('[value='+val+']').text().replace(/\s*\[[^\]]+\]$/, '') })
			})
		} else {
			option.values.push({ id:val, name:$option.find('[value='+val+']').text().replace(/\s*\[[^\]]+\]$/, '') })
		}
		options.push(option)
	})

	item.id = Math.floor(Math.random()*1000000000000).toString()
	item.item = item_data.item
	item.name = $item.find('h1').text()
	item.options = options
	item.quantity = parseInt(item_data.quantity)
	item.subtotal = parseFloat($item.find('.price').text().replace('$', ''))
	item.total = item.subtotal * item.quantity
	item.instructions = $item.find('[name=instructions]').val()

	this.items[item.id] = item

	return item
}

Order.prototype.renderNewItem = function(restaurant, item) {
	var order = this
	  , items_list = this.$el.find('.restaurant[data-restaurant='+restaurant.restaurant+'] .items_list')
	
	if(items_list.size()) {
		dust.render('order-item', item, function(err, html) {
			items_list.append(html)
		})
	} else {
		dust.render('order-restaurant', restaurant, function(err, html) {
			order.$el.find('.restaurants_list').append(html)
		})
	}
}

Order.prototype.deleteItem = function(e) {
	e.preventDefault()

	var $item_element = $(e.target).closest('.item')
	  , $restaurant_element = $item_element.closest('.restaurant')
	  , item_id = $item_element.attr('data-id')
	  , restaurant_id = $restaurant_element.attr('data-restaurant')
	  , restaurant = this.getRestaurant(restaurant_id)
	  , item = this.getItem(restaurant, item_id, true)
	  , remaining_restaurants

	this.subtotal -= item.total
	this.quantity -= item.quantity

	delete this.items[item.id]

	if(this.quantity) {
		$('a[href="/checkout"].checkout').show()
	} else {
		$('a[href="/checkout"].checkout').hide()
	}

	if(!restaurant.items.length) {
		this.getRestaurant(restaurant_id, true)
		remaining_restaurants = this.restaurants.length
		$restaurant_element.velocity('slideUp', { complete:function() {
			$restaurant_element.remove()
			if(!remaining_restaurants) $('body').removeClass('order-open')
		} })
	} else {
		$item_element.velocity('slideUp', { complete:function() {
			$item_element.remove()
		} })
	}

	this.updateDOM()
	this.store()

	loader.preload('/checkout', true)
	loader.preload('/checkout?drinks=false', true)
	loader.preload('/checkout/guest', true)
	loader.preload('/checkout/guest?drinks=false', true)
}

Order.prototype.submitQty = function(e) {
	var $qty = $(e.target)
	$qty.val(Math.min((Math.max(1, Math.floor($qty.val())) || 1), 99))
	$qty.closest('form').submit()
}

Order.prototype.updateQty = function(e) {
	var $form = $(e.target)
	  , $quantity = $form.find('input.quantity')
	  , $item_element = $quantity.closest('.item')
	  , item_id = $item_element.attr('data-id')
	  , item = this.items[item_id]

	this.subtotal -= item.total
	this.quantity -= item.quantity

	item.quantity = parseInt($quantity.val())
	item.total = item.subtotal * item.quantity

	this.subtotal += item.total
	this.quantity += item.quantity

	$item_element.find('.total').text(currency.format(item.total))

	this.updateDOM()
	this.store()

	loader.preload('/checkout', true)
	loader.preload('/checkout?drinks=false', true)
	loader.preload('/checkout/guest', true)
	loader.preload('/checkout/guest?drinks=false', true)

	e.preventDefault()
}

Order.prototype.updateDOM = function() {
	this.subtotal_element.text(currency.format(this.subtotal))
	this.quantity_element.text(this.quantity)
	this.toggle_quantity_element.text(this.quantity)

	if(!this.quantity) {
		$('a[href="/checkout"].checkout').hide()
	} else {
		$('a[href="/checkout"].checkout').show()
	}
}

Order.prototype.empty = function() {
	this.restaurants = []
	this.items = {}
	this.quantity = 0
	this.subtotal = 0
	this.$el.find('.restaurant').remove()
	this.updateDOM()
}

modules.register('order', Order)