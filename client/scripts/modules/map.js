var modules = require('./')
  , Map = function($el) {
	var location_parts = $el.attr('data-location').split(',')
	  , position = new google.maps.LatLng(parseFloat(location_parts[0]), parseFloat(location_parts[1]))

  	this.map = new google.maps.Map($el[0], {
		  disableDefaultUI:true
		, zoom:17
		, center:position
		, scrollwheel: false
	    , navigationControl: false
	    , mapTypeControl: false
	    , scaleControl: false
	    , draggable: false
	})

	this.marker = new google.maps.Marker({
		  map:this.map
		, position:position
	})

}

modules.register('map', Map)

Map.prototype.center = function(lat, lng) {
	var position = new google.maps.LatLng(lat, lng) 
	this.map.setCenter(position)
	this.marker.setPosition(position)
}