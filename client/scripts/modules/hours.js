var modules = require('./')

modules.register('hours', function($el) {
	var img = $el.add('<img>').attr('src', 'http://triviaforgood.com/images/gpl/icons/icons-white/11-clock@2x.png')
	  , current = $el.add('<div class="current_status">')
	  , restaurant_hours
	  , status

	$el.find('h1').each(function() { 
		var title = $el.find(this)
		  , hours
		  , container
		if(title.text().trim() == 'Opening Hours' && !title.closest('.restaurant_hours').size()) {
			hours = title.next('p')
			container = $el.add('<div class="restaurant_hours">')
			title.before(container)
			container.append(title).append(hours)
		}
	})

	restaurant_hours = $el.find('.restaurant_hours')

	if(restaurant_hours.size()) {
		status = getCurrentStatus()
		current.addClass(status.open ? 'open' : 'close')
		       .append(img)
		       .append($el.add('<span>').html(status.text))
		       .insertBefore(restaurant_hours)
		       .click(function() {
		       		restaurant_hours.slideToggle()
	      		})
	}

	function getCurrentStatus(hours_text) {
		var weekly_hours = getHours(hours_text)
		  , today = new Date()
		  , day = today.getDay()
		  , next_hours
		  , todays_hours
		  , text
		  , open = false

		if(weekly_hours[day]) {
			todays_hours = getOpenClose(weekly_hours[day])
			if(today > todays_hours.open && today < todays_hours.close) {
				open = true
				text = 'Open until '+todays_hours.close_string
			} else if(today < todays_hours.open) {
				text = 'Closed until '+todays_hours.open_string
			} else {
				next_hours = getOpenClose(getNextOpenDay(weekly_hours, day))
				text = 'Closed until '+next_hours.day+' at '+next_hours.open_string
			}
		} else {
			next_hours = getOpenClose(getNextOpenDay(weekly_hours, day))
			text = 'Closed until '+next_hours.day+' at '+next_hours.open_string
		}

		return { open:open, text:text }
	}
	function getOpenClose(day) {
		var open = new Date()
		  , close = new Date()
		open.setHours(day.open.hours)
		open.setMinutes(day.open.minutes)
		open.setSeconds(0)
		close.setHours(day.close.hours)
		close.setMinutes(day.close.minutes)
		close.setSeconds(0)
		if(close < open) {
			close.setDate(close.getDate()+1)
		}
		console.log(open, close)
		return { day:day.day, open:open, close:close, open_string:day.open_string, close_string:day.close_string }
	}

	function getNextOpenDay(hours, i) {
		var day, original = i

		i = (i+1)%7

		while(i != original) {
			if(hours[i]) 
				return hours[i]
			i = (i+1)%7
		}
	}

	function getHours(text) {
		var full_text = text
		  , hours_regex = /((?:Mon|Tues|Wednes|Thurs|Fri|Satur|Sun)day)\s*(\d+\:\d{2}\s*(?:am|pm)?)\s*.\s*(\d+\:\d{2}\s*(?:am|pm)?)/gi
		  , days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']
		  , hours = []
		  , match
		while(match = hours_regex.exec(full_text)) {
			hours[days.indexOf(match[1].toLowerCase())] = {
				  day: match[1]
				, open: getHoursMinutes(match[2])
				, open_string: match[2]
				, close: getHoursMinutes(match[3])
				, close_string: match[3]
			}
		}
		return hours
	}

	function getHoursMinutes(time) {
		var parts = /(\d+)\:(\d{2})\s*(am|pm)?/i.exec(time)
		  , hours = (parseInt(parts[1]) + (!parts[3] || parts[3].toLowerCase() == 'am' ? 0 : 12)) % 24
		  , minutes = parseInt(parts[2])

		if(parts[3] && parts[3].toLowerCase() == 'am' && hours == 12) {
			hours = 0
		}
		return { hours:hours, minutes:minutes }
	}

	function findOrCreate(selector, element) {
		selector
	}

})
