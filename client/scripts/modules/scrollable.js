var modules = require('./')

modules.register('scrollable', function($el) {
	var touched
	  , focused

	$el.on('focus', 'input:not(:radio,:checkbox),textarea', function() {
		if(touched == this) {
			waitForResize($(this), 500)
		} else {
			scrollIntoView($(this))
		}
	}).on('touchend', 'input:not(:radio,:checkbox),textarea', function() {
		touched = this
		focused = $(document.activeElement).filter('input:not(:radio,:checkbox),textarea')[0]
		setTimeout(function() {
			touched = null
			focused = null
		}, 500)
	})

	$(window).on('resize', function() {
		var $active = $(document.activeElement)
		if($active.parents().filter($el).size()) {
			scrollIntoView($active)
		}
	})

	function waitForResize($this, max) {
		var timeout

		if(focused && window.screen.height-window.outerHeight > 150) {
			return scrollIntoView($this)
		}

		$this.css('top', -10000)
		timeout = setTimeout(resize, max)
		$(window).one('resize', resize)
		
		function resize(e) {
			$this.css('top', 0)
			window.requestAnimationFrame(function() {
				scrollIntoView($this)
			})
			clearTimeout(timeout)
		}
	}

	function clone($active) {
		var $clone = $active.clone()
		  , pos = $active.position()

		$clone.css({ 
			  position:'absolute'
			, top:pos.top
			, left:pos.left
		}).insertAfter($active)
		
		return $clone
	}

	function scrollIntoView($active) {
		var scrollTop = $active.position().top+$active.height()/2
		  , $offsetParent = $active.offsetParent()

		while($offsetParent[0] != $el[0]) {
			scrollTop += $offsetParent.position().top //-$offsetParent.height()/2
			$offsetParent = $offsetParent.offsetParent()
		} 
		$el.stop().animate({
			scrollTop:scrollTop+$el.scrollTop()-$el.height()/3
		}, 400)
	}

})