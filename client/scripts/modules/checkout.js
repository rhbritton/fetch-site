var modules = require('./')
  , loader = require('../loader')
  , Order = require('./order')

modules.register('checkout', function($el) {
	var $payment = $el.find('[data-module*=payment]')
	  , $new_card = $payment.find('.new.card input')
	  , $card_href = $payment.find('#cc-href')
	  , $card = $payment.find('.type_select .option.card input')
	  , $submit = $el.find('[type=submit]')

	$el.submit(function(e) {
		$submit.text('Placing order...').attr('disabled', 'disabled')

		if($card.is(':checked') && !$card_href.val()) {
			$payment.module().createCard(function(data) {
				if(data.status_code == 201) loader.submit($el)
			})
			e.preventDefault()
		}
		$('nav.order').module().empty()
	})

})