var modules = require('./')

modules.register('phone', function($el) {

	$el.blur(function() {
		$el.val(
			$el.val().replace(/[^\d+!x]/gi, '')
				.replace(/(\d*)(\d{3})(\d{3})(\d{4})/, '$1-$2-$3-$4')
				.replace(/^(\+)?-/, '$1')
				.replace(/x/, ' x')
		)
	})

})