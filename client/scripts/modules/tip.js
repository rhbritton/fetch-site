var modules = require('./')

modules.register('tip', function($el) {

	var $amount = $el.find('[name="tip"]')
	  , $percent = $el.find('.percent')
	  , total = parseFloat($('.summary .total .amount').attr('data-amount'))

	if($amount.attr('data-body') !== undefined) {
		if(5/total*100 > 15) {
			$percent.val('custom')
		} else {
			$percent.val(15)
			$amount.val((total*parseFloat($percent.val())/100 || 0).toFixed(2))
		}
	}

	$percent.change(function() {
		if($(this).val() != 'custom') {
			$amount.val((total*parseFloat($percent.val())/100 || 0).toFixed(2))
		}
	})

	$amount.keyup(function() {
		$percent.val('custom')
	})
	$amount.change(function() {
		$percent.val('custom')
		$amount.val((parseFloat($amount.val()) || 0).toFixed(2))
	})

})