var modules = require('./')

modules.register('defer-images', function($el) {
	var $scroll = $el.closest('[data-module*=scrollable]')
	  , timeout

	/*$scroll.on('scroll', function() {
		clearTimeout(timeout)
		timeout = setTimeout(hideShow, 30)
	})*/

	function hideShow() {
		var show = []
		  , hide = []
		$el.find('img').parent().each(function() {
			var $this = $(this)
			if(isOnScreen($this)) {
				show.push($this.find('img'))
			} else {
				hide.push($this.find('img'))
			}
		})

		show.forEach(function($el) {
			$el.show()
		})

		hide.forEach(function($el) {
			$el.hide()
		})
	}

	function isOnScreen($el) {
		var scroll_height = $scroll.height()
		  , scroll_top = $scroll.offset().top
		  , element_top = $el.offset().top
          , element_height = $el.height()

        return (element_top < scroll_height + scroll_top) && 
               (element_top + element_height > -1 * scroll_top)
	}

})

