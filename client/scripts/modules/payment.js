var modules = require('./')
  , Payment

Payment = function($el) {
	this.$el = $el
	this.$cc_name = this.$el.find('#cc-name')
	this.$cc_number = this.$el.find('#cc-number')
	this.$cc_month = this.$el.find('#cc-expiration_month')
	this.$cc_year = this.$el.find('#cc-expiration_year')
	this.$cc_cvv = this.$el.find('#cc-cvv')
	this.$type_select = this.$el.find('.type_select')
	this.$cards = this.$el.find('.cards')
	this.$new_card_input = this.$cards.find('.new.card input')
	this.$cards_radios = this.$cards.find('.card input[type=radio]')
	this.$cards_radios.change(this.selectCard.bind(this))
	this.validateCardOnChange()
	this.toggleCardDropdown()
}

Payment.prototype.selectCard = function() {
	var $selected = this.$cards_radios.filter(':checked')
	this.$cards_radios.closest('.card').removeClass('selected')
	$selected.closest('.card').addClass('selected')
}

Payment.prototype.toggleCardDropdown = function() {
	var payment = this
	payment.$type_select.find('input').change(function(e) {
		var $selected = payment.$type_select.find('input:radio[name=type]:checked').closest('.option')
		payment.$type_select.find('.option').removeClass('selected')
		$selected.addClass('selected')
		if($selected.hasClass('cash')) {
			payment.requireCardFields(false)
			payment.$cards.hide()
		}
		if($selected.hasClass('card')) {
			payment.requireCardFields(true)
			payment.$cards.show()
			payment.$el.closest('.content[role=main]').animate({
				scrollTop:payment.$el.position().top+payment.$el.closest('.content[role=main]').scrollTop()
			})
		}
	})
}

Payment.prototype.requireCardFields = function(required) {
	var fields = [
		  this.$cc_name
		, this.$cc_number
		, this.$cc_month
		, this.$cc_year
		, this.$cc_cvv
	]

	fields.forEach(function($field) {
		if(required) {
			$field.change().blur().attr('required', true)
		} else {
			$field.removeAttr('required')
			$field[0].setCustomValidity('')
		}
	})
}

Payment.prototype.validateCardOnChange = function() {
	var payment = this

	payment.$cc_number.focus(function() {
		payment.$cc_number.val(payment.$cc_number.attr('data-number'))
	})

	payment.$cc_number.blur(function() {
		var num = payment.$cc_number.val()
		  , stored_num = payment.$cc_number.attr('data-number')
		if(/[^\d\s\w]+\d{4}/.test(num) && num && stored_num && num.slice(-4) == stored_num.slice(-4)) return
		payment.$cc_number.attr('data-number', num)
		if(num && !balanced.card.isCardNumberValid(num)) {
			this.setCustomValidity('Invalid Credit Card Number')
		} else {
			this.setCustomValidity('')
		}

		num = num.replace(/[^\d]/g, '')
		if(num.length > 4) {
			payment.$cc_number.val(Array(num.length-4).join('\u00D7')+num.slice(-4))
		}
	})

	payment.$cc_cvv.change(function() {
		if(payment.$cc_number.val()) {
			if(!balanced.card.isCVVValid(payment.$cc_number.attr('data-number'), payment.$cc_cvv.val())) {
				this.setCustomValidity('Invalid CVV')
			} else {
				this.setCustomValidity('')
			}
		}
	})

	this.$el.find('#cc-expiration_month, #cc-expiration_year').change(function() {
		if(payment.$cc_month.val() && payment.$cc_year.val()) {
			if(!balanced.card.isExpiryValid(payment.$cc_month.val(), payment.$cc_year.val())) {
				payment.$cc_year[0].setCustomValidity('Expiration date must be in future')
			} else {
				payment.$cc_year[0].setCustomValidity('')
			}
		} else {
			payment.$cc_year[0].setCustomValidity('')
		}
	})
}

Payment.prototype.createCard = function(fn) {
	var payment = this
	  , payload = {
	    	name: this.$cc_name.val(),
	    	number: this.$cc_number.attr('data-number'),
	    	expiration_month: this.$cc_month.val(),
	    	expiration_year: this.$cc_year.val(),
	    	security_code: this.$cc_cvv.val()
	    }

	balanced.card.create(payload, function(data) {
		if(data.status_code == 201) {
			console.log('card', payment.$el.find('#cc-href').size(), data.cards[0].href)
			payment.$el.find('#cc-href').val(data.cards[0].href)
		} 
		fn(data)
	})
}

modules.register('payment', Payment)