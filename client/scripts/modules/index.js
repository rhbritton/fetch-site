var modules = {}

$.fn.module = function() {
	return this.data('module')
}

module.exports.register = function(name, module) {
	modules[name] = module
}

module.exports.init = function($element, include_container) {
	if($element.attr('data-module') && include_container) 
		loadModules($this)

	$element.find('[data-module]').each(function() {
		loadModules($(this))
	})
}

function loadModules($element) {
	$element.attr('data-module').split(' ').forEach(function(name) {
		var m = modules[name]
		if(m) $element.data('module', new m($element))
		else console.log('MISSING MODULE: '+name)
	})
}

// require modules
require('./location')
require('./navigation')
require('./order')
require('./item-order')
require('./map')
require('./payment')
require('./tip')
require('./form')
require('./checkout')
require('./summary')
require('./phone')
require('./contact')
require('./address')
require('./hours')
require('./scrollable')
require('./defer-images')
