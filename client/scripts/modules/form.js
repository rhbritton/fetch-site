var modules = require('./')

modules.register('form', function($el) {

	$el.on('blur', 'input, textarea', validate)
	$el.on('change', 'select', validate)
	
	function validate() {
		var self = this
		
		if($(self).is(':visible')) {
			setTimeout(function() {
				var $error = $(self).next('span.error')
				if(self.willValidate) {
					if(!self.checkValidity()) {
						if(!$error.size()) {
							$error = $('<span class="error">')
							$error.insertAfter(self)
						}
						return $error.text(self.validationMessage)
					}
				}
				$error.remove()
			}, 0)
		}
		
	}

})