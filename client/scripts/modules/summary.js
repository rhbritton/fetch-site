var modules = require('./')

modules.register('summary', function($el) {

	$el.find('.reveal').click(function(e) {
		$el.find('.reveal').velocity('slideUp')
		$el.find('ul.order').velocity('slideDown')
		e.preventDefault()
	})

})