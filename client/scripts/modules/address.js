var modules = require('./')

modules.register('address', function($el) {
	
	$el.find('.change_address').click(function(e) {
		e.preventDefault()
		$('.location.modal').module().open(window.location.pathname != '/restaurants', function(new_address) {
			if($el.find('.map').length) {
				new_address.loc = new_address.loc.split(',')
				$el.find('.map').module().center(new_address.loc[0], new_address.loc[1])
			}
			$el.find('address span').html(new_address.street + '<br>' + new_address.city + ' ' + new_address.state + ' ' + new_address.zip)
		})
	})

})