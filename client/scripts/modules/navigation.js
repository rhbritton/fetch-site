var modules = require('./')
  , loader = require('../loader')
  , routes = require('../../../util/routes')
  , indicator_timeout
  , $body = $(document.body)
  , $order = $('nav.order')
  , $menu = $('nav[role=main]')
  , $fader = $('#wrapper .fader')
  , $menu_button = $('header[role=main] a.menu')
  , $back_button = $('header[role=main] a.back')

modules.register('navigation', function($el) {

	$el.find('a.menu').click(function() {
		if(!$body.hasClass('menu-open')) {
			$order.hide()
			$menu.show().scrollTop(0)
			$body.addClass('menu-open')
		} else {
			$body.removeClass('menu-open')
		}
	})
	$el.find('a.order').click(function() {
		if(!$body.hasClass('order-open')) {
			$menu.hide()
			$order.show().scrollTop(0)
			$body.addClass('order-open')
		} else {
			$body.removeClass('order-open')
		}
	})
	$fader.click(function() {
		$body.removeClass('order-open menu-open')
	})
	$back_button.click(function(e) {
		var up = routes.up(window.location.pathname)
		if(/^\/checkout/.test(window.location.pathname)) {
			history.go(-1)
		} else {
			if(history.state && history.state.previous == up) history.go(-1)
			else loader.load(up)
		}
		e.preventDefault()
	})
})

loader.on('start', function(url, prev, nopush) {
	$body.removeClass('order-open menu-open failed')
	indicator_timeout = setTimeout(function() {
		$body.addClass('loading')
	}, 100)
})

loader.on('load', function($content, url, prev) {
	clearTimeout(indicator_timeout)
	$body.removeClass('loading')
})

loader.on('static', function() {
	$body.removeClass('order-open menu-open failed')
})

loader.on('fail', function($container, url, prev, xhr) {
	$body.addClass('failed')
	console.log(xhr)
	$body.find('.loading .error h1').html(xhr.status ? xhr.status + ': ' + xhr.statusText : 'Could not reach server')
})