module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		dust: {
			compile: {
				files: [{
					expand: true,
					cwd: "templates/",
					src: ["**/*.dust"],
					dest: "build/templates/",
					ext: ".js"
				}],
				options: {
					relative: true,
					wrapper: 'commonjs',
					wrapperOptions: {
						deps: {
							dust: "../dust-runtime"
						}
					}
				}
			}
		},
		browserify: {
			compile: {
				files: {
					'build/scripts.js': ['client/scripts/start.js']
				}
			}
		},
		stylus: {
			compile: {
				options: {
					import: [ 'nib' ]
				},
				files: {
					'build/styles.css': ['client/styles/**/*.styl']
				}
			}
		},
		imagemin: {
			compress: {
				options: {
					pngquant:true,
					cache:false
				},
				files: [{
					expand: true,
					cwd: 'client/assets/images/',
					src: ['**/*.{png,jpg,gif}'],
					dest: 'build/assets/images/'
				}]
			}
		},
		watch: {
			styles: {
				files: ['client/styles/**/*.styl'],
				tasks: ['stylus']
			},
			scripts: {
				files: ['client/scripts/**/*.js'],
				tasks: ['browserify:compile']
			},
			templates: {
				files: ['templates/**/*.dust'],
				tasks: ['dust', 'browserify:compile']
			},
			images: {
				files: ['client/assets/images/**/*.{png,jpg,gif}'],
				tasks: ['imagemin']
			}
		}
	})

	grunt.loadNpmTasks('grunt-dust')
	grunt.loadNpmTasks('grunt-browserify')
	grunt.loadNpmTasks('grunt-contrib-stylus')
	grunt.loadNpmTasks('grunt-contrib-watch')
	grunt.loadNpmTasks('grunt-contrib-imagemin')

	grunt.registerTask('default', ['dust', 'browserify', 'stylus', 'imagemin', 'watch'])

}